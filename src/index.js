import * as THREE from 'three';
import * as CANNON from 'cannon-es'; //Physics engine
import cannonDebugger from 'cannon-es-debugger' //Physics engine debugger

import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { VRButton } from 'three/examples/jsm/webxr/VRButton.js';
import { XRControllerModelFactory } from 'three/examples/jsm/webxr/XRControllerModelFactory.js';
import { ConvexHull } from 'three/examples/jsm/math/ConvexHull.js';
import {QuickHull} from 'quickhull/quickhull.js';
import { ConvexGeometry } from 'three/examples/jsm/geometries/ConvexGeometry.js';


import { heartsController } from './hearts.js';

import roomModel from './assets/night-studies.gltf';
import wandModel from './assets/wand.gltf';
import backgroundTex from './assets/rooftop_night.jpg';
import { NormalMapTypes } from 'three';



class Main {

    wand;
    room;
    hearts;
    constructor() {
        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera(60, window.innerWidth / window.innerHeight, 0.1, 3000);
        this.camera.position.set(100, 100, 100);

        const renderer = new THREE.WebGLRenderer({ antialias: true });
        renderer.xr.enabled = true;
        renderer.setSize(window.innerWidth, window.innerHeight);
        renderer.setClearColor(0xEEEEEE);
        renderer.shadowMap.enabled = true;
        renderer.shadowMap.type = THREE.PCFSoftShadowMap;
        this.renderer = renderer;
        document.body.appendChild(renderer.domElement);
        document.body.appendChild(VRButton.createButton(renderer));

        this.loader = new GLTFLoader();
        this.loader2 = new GLTFLoader();

        this.controls = new OrbitControls(this.camera, this.renderer.domElement);
        this.controls.enableKeys = false;

        this.clock = new THREE.Clock();

        //to see physics wireframe and create a test body
        this.debugPhysics = true;

        this.initPhysics();

        this.createBackground()

        this.createLight();
        this.createMagicWand();
        this.createRoom();
        this.createControllers();
        this.createBackground();
        this.setupEvents();


        //physics related stuff

        //to see physics wireframe and create a test body
        this.debugPhysics = true;
        this.createGround();

        if (this.debugPhysics) {
            this.addTestBody();
        }

        this.animate();
    }

    initPhysics() {
        //creates a world for physics operations
        this.world = new CANNON.World();
        this.world.broadphase = new CANNON.NaiveBroadphase;
        this.world.gravity.set(0, -9.82, 0); // m/s²  

        //Sets up a debugger, aka the wireframe that is drawn.
        if (this.debugPhysics) {
            this.debugColor = new THREE.Color(0xff0000);
            cannonDebugger(this.scene, this.world.bodies, {color: this.debugColor});
        }

        // Tweak contact properties.
        // Contact stiffness - use to make softer/harder contacts
        this.world.defaultContactMaterial.contactEquationStiffness = 1e6;

        // Stabilization time in number of timesteps
        this.world.defaultContactMaterial.contactEquationRelaxation = 10;

        // Max solver iterations: Use more for better force propagation, but keep in mind that it's not very computationally cheap!
        this.world.solver.iterations = 5;

    }

    createGround() {
        //creates a physics body that is a plane
        const groundShape = new CANNON.Plane();
        this.groundMaterial = new CANNON.Material();
        const groundBody = new CANNON.Body({ mass: 0, material: this.groundMaterial }); //mass 0 means the body never moves 
        groundBody.quaternion.setFromAxisAngle(new CANNON.Vec3(1, 0, 0), - Math.PI / 2);
        groundBody.addShape(groundShape);
        this.world.addBody(groundBody);
    }

    addTestBody() {
        //Creates a test body in the scene to check physics
        const shape = new CANNON.Sphere(1);
        const bouncyMaterial = new CANNON.Material('bouncy');
        const testBody = new CANNON.Body({ mass: 20, material: bouncyMaterial });
        testBody.addShape(shape);
        testBody.position.set(2, 2, 2);
        testBody.velocity.set(0, 10, 0);
        testBody.linearDamping = 0;

        this.world.addBody(testBody);

        //Sets up material properties
        const contact = new CANNON.ContactMaterial(bouncyMaterial, testBody, { friction: 0.0, restitution: 0.7 });
        this.world.addContactMaterial(contact);
    }

    setupEvents() {
        window.addEventListener('resize', () => {
            this.camera.aspect = window.innerWidth / window.innerHeight;
            this.camera.updateProjectionMatrix();
            this.renderer.setSize(window.innerWidth, window.innerHeight);
        });
    }


    createMagicWand() {
        var wandGroup = new THREE.Group();
        this.wandGroup = wandGroup;
        this.scene.add(wandGroup);

        this.loader.load(wandModel, (gltf) => {
            this.wand = gltf.scene;
            this.wand.name = "wand";
            this.wand.scale.x = 0.2;
            this.wand.scale.y = 0.2;
            this.wand.scale.z = 0.2;
            this.wand.castShadow = true;
            this.wand.receiveShadow = true;
            this.scene.add(this.wand);
            this.wandGroup.add(this.wand);
        }, undefined, function (error) {
        });
        this.wand = this.scene.getObjectByName("wand");

        //making anchor for particles
        const geometry = new THREE.SphereGeometry(0.05, 32, 32);
        const material = new THREE.MeshBasicMaterial({ color: 0xffff00 });
        const particleAnchor = new THREE.Mesh(geometry, material);
        particleAnchor.name = "particleAnchor";
        this.scene.add(particleAnchor);
        this.wandGroup.add(particleAnchor);
        particleAnchor.position.set(0, 0.65, 0);
        this.particleAnchor = this.scene.getObjectByName("particleAnchor");
    }



    createRoom() {
        this.loader2.load(roomModel, (gltf) => {
            this.room = gltf.scene;
            this.room.scale.x = 0.7;
            this.room.scale.y = 0.7;
            this.room.scale.z = 0.7;
            this.room.name = "room";
            this.room.traverse(function (child) {
                if (child.name == "floor")
                {
                    child.receiveShadow = true;
                } else {
                    child.castShadow = true;
                    child.receiveShadow = true;
                }

            });
            this.scene.add(this.room);
            console.log("Creating room");
            //this.setUpRoomPhysics(this.room,this.world);
        },
            undefined, function (error) {

            });

        //this.room = this.scene.getObjectByName("room");

    }

    setUpRoomPhysics(room,world){
        //this.world = world;
        //this.room = room;
        console.log("Starting to add physics objects");
        this.room.traverse(function (child) {
            if (child.isMesh) {
                console.log(child.geometry);
                //const buffGeo = new THREE.BufferGeometry(child.geometry);
                //console.log(buffGeo);
                //const geometry = new ConvexGeometry(buffGeo);
                console.log(child.name);
                let hull = new ConvexHull()
                hull.setFromObject(child);
                //let geom = new THREE.ConvexGeometry(hull); 
                //const hull = new ConvexHull(child.geometry.attributes); //Varbūt THREE.ConvexHull
                const points = hull.vertices.map(v => new CANNON.Vec3(v.point.x, v.point.y, v.point.z));
                let geom = new ConvexGeometry(hull.vertices.map(v => v.point)); //Varbūt THREE.ConvexGeometry
                
                let mesh = new THREE.Mesh(geom, new THREE.MeshBasicMaterial({
                    color: 0x00ff00,
                    wireframe: true
                }));
                mesh.scale.divide(mesh.scale);
                //room.add(mesh);

                console.log(hull);

                const faces = hull.faces.map(f => [
                    hull.vertices.indexOf(f.getEdge(0).vertex),
                    hull.vertices.indexOf(f.getEdge(1).vertex),
                    hull.vertices.indexOf(f.getEdge(2).vertex)]);
                var shape = new CANNON.ConvexPolyhedron({vertices: points, faces});

                var body = new CANNON.Body({ mass: 1 });
                body.addShape(shape);
                world.addBody(body);
            }
        });
    }

    createControllers() {
        var controllerModelFactory = new XRControllerModelFactory();

        const controllerGrip1 = this.renderer.xr.getControllerGrip(0);
        const controllerGrip2 = this.renderer.xr.getControllerGrip(1);

        const model1 = controllerModelFactory.createControllerModel(controllerGrip1);
        const model2 = controllerModelFactory.createControllerModel(controllerGrip2);

        controllerGrip1.add(model1);
        controllerGrip2.add(model2);

        this.scene.add(controllerGrip1);
        this.scene.add(controllerGrip2);

        const controller1 = this.renderer.xr.getController(0);
        const controller2 = this.renderer.xr.getController(1);

        //These variables are used to get controller data for later operations
        this.controller1 = controller1;
        this.controller2 = controller2;

        //fire some rays from controllers
        const geometry = new THREE.BufferGeometry().setFromPoints([new THREE.Vector3(0, 0, 0), new THREE.Vector3(0, -5, - 1)]);

        const line = new THREE.Line(geometry);
        line.name = 'line';
        line.scale.z = 5;

        controllerGrip1.add(line.clone());
        controllerGrip2.add(line.clone());

        this.hearts = new heartsController(this.scene, this.controller1);
        //this.controller1.add(this.hearts);



        //raycaster = new THREE.Raycaster();

    }

    createBackground() {
        const geometry = new THREE.SphereGeometry(2000 + 10, 32, 32);
        const material = new THREE.MeshStandardMaterial({
            map: new THREE.TextureLoader().load(backgroundTex),
            side: THREE.DoubleSide
        });

        this.skysphere = new THREE.Mesh(geometry, material);
        this.scene.add(this.skysphere);
    }

    createLight() {
        const skyColor = 0xB1E1FF;  // light blue
        const groundColor = 0xB97A20;  // brownish orange
        const intensity = 1.2;
        const light = new THREE.HemisphereLight(skyColor, groundColor, intensity);
        this.scene.add(light);



        const directionalLight = new THREE.DirectionalLight(0xf791e8, 0.7); //pink
        directionalLight.position.set(-1.5, 1, 0.2);
        directionalLight.castShadow = true;
		directionalLight.shadow.camera.near =  0.5;
		directionalLight.shadow.camera.far = 10;

		directionalLight.shadow.mapSize.width = 1024;
		directionalLight.shadow.mapSize.height = 1024;
        this.scene.add(directionalLight);
    }



    animate() {
        this.scene.updateMatrixWorld;
        //updates physics
        let delta = this.clock.getDelta();
        if (delta > 0) {
            this.world.step(delta);
        }


        //Checks if controller has been created and wand has been added to the scene
        if (this.wand != undefined && this.controller1 != undefined) {
            //console.log(this.world);

            this.wandGroup.position.x = this.controller1.position.x;
            this.wandGroup.position.y = this.controller1.position.y;
            this.wandGroup.position.z = this.controller1.position.z;

            this.wandGroup.rotation.x = this.controller1.rotation.x;
            this.wandGroup.rotation.y = this.controller1.rotation.y;
            this.wandGroup.rotation.z = this.controller1.rotation.z;

            this.hearts.animate(this.particleAnchor);

        }

        this.controls.update();
        this.renderer.setAnimationLoop(() => this.animate());
        this.renderer.render(this.scene, this.camera);
    }



}

(new Main());