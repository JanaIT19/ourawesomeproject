import * as THREE from 'three';
import heartTex from './assets/heart.png';
//import heartTex from './assets/comrade.png';
export class heartsController {
    hearts_geom = new THREE.BufferGeometry();
    particles;

    constructor(scene) {
        let heartTexture = new THREE.TextureLoader().load(heartTex);
        let heartMaterial = new THREE.PointsMaterial({
            size: 0.05, map: heartTexture, transparent: true, depthWrite: false, blending: THREE.AdditiveBlending
        });
 
        //used later for rotation and positioning of particles
        
        const globalPos = new THREE.Vector3(0,0,0);
        this.globalPos = globalPos;

        const globalRot = new THREE.Quaternion(0,0,0,0);
        this.globalRot = globalRot;

        const positions = [];
        const sizes = [];
        const radius = 0.5;

        for (let p = 0; p < 15; p++) {
            positions.push( radius * Math.random() - radius / 2 );
            positions.push( radius * Math.random() - radius / 2  );
            positions.push( radius * Math.random() - radius / 2  );
            
            sizes.push(15);
        }

        this.hearts_geom.setAttribute( 'position', new THREE.Float32BufferAttribute( positions, 3 ) );
		this.hearts_geom.setAttribute( 'size', new THREE.Float32BufferAttribute( sizes, 1 ).setUsage( THREE.DynamicDrawUsage ) );
        
        this.particles = new THREE.Points(this.hearts_geom, heartMaterial);
        
        scene.add(this.particles);
    }
 
    animate(anchor) {
        const time = Date.now() * 0.005;
        
        anchor.getWorldQuaternion(this.globalRot);

        this.particles.rotation.z = this.globalRot.z;
        this.particles.rotation.x = this.globalRot.x;
        this.particles.rotation.y = 0.04 * time + this.globalRot.y;
        

        anchor.getWorldPosition(this.globalPos);
        this.particles.position.z = this.globalPos.z;
        this.particles.position.x = this.globalPos.x;
        this.particles.position.y = this.globalPos.y;

        const sizes = this.hearts_geom.attributes.size.array;

        for ( let i = 0; i < 15; i ++ ) {

            sizes[ i ] = (Math.random() * 50) * ( 5 + Math.sin( 0.1 * i + time ) );

        }

        this.hearts_geom.attributes.size.needsUpdate = true;
    }
 }
